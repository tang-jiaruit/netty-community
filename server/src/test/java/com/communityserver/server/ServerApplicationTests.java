package com.communityserver.server;


import com.communityserver.server.controller.ControllerManage;
import com.rabbitmq.client.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import static com.community.common.config.rabbitmq.RabbitMqConfig.*;

@SpringBootTest()
@RunWith(SpringRunner.class)
class ServerApplicationTests {

    @Resource
    RabbitTemplate rabbitTemplate;

    @Test
    void test(){
        try {
            ConnectionFactory connectionFactory = new ConnectionFactory();
            connectionFactory.setHost(RABBITMQ_HOST);
            connectionFactory.setPort(RABBITMQ_PORT);
            connectionFactory.setUsername(RABBITMQ_USERNAME);
            connectionFactory.setPassword(RABBITMQ_PASSWORD);
            connectionFactory.setVirtualHost(RABBITMQ_VIRTUALHOST);
            connectionFactory.setConnectionTimeout(RABBITMQ_TIMEOUT);

            Channel channel = connectionFactory.newConnection().createChannel();
            channel.basicPublish(MESSAGE_DIRECT_EXCHANGE,"user_message_9",null,"你好".getBytes(StandardCharsets.UTF_8));

//            rabbitTemplate.convertAndSend(MESSAGE_DIRECT_EXCHANGE,"user_message_9","你好");
//            ControllerManage.handleReq("user","selectUser","2000043401");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testRabbitmq(){
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        // 设置RabbitMQ服务器的连接信息
        connectionFactory.setHost(RABBITMQ_HOST);
        connectionFactory.setPort(RABBITMQ_PORT);
        connectionFactory.setUsername(RABBITMQ_USERNAME);
        connectionFactory.setPassword(RABBITMQ_PASSWORD);
        connectionFactory.setVirtualHost(RABBITMQ_VIRTUALHOST);
        connectionFactory.setConnectionTimeout(RABBITMQ_TIMEOUT);
        Channel channel = connectionFactory.createConnection().createChannel(false);
        try {
            // 开启确认模式
//            channel.confirmSelect();

            // 发送消息
            channel.basicPublish(MESSAGE_DIRECT_EXCHANGE, "user_message_9", null, "你好".getBytes(StandardCharsets.UTF_8));

//            // 等待确认
//            try {
//                if (channel.waitForConfirms()) {
//                    System.out.println("Message sent successfully!");
//                } else {
//                    System.out.println("Failed to send message!");
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    void contextLoads() {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "test message, hello!";
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String,Object> map=new HashMap<>();
        map.put("messageId",messageId);
        map.put("messageData",messageData);
        map.put("createTime",createTime);
        rabbitTemplate.convertAndSend("TestExchange","testRoute",map);
    }

    @Test
    void consumer() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();

        factory.setHost("39.108.110.150");
        factory.setUsername("root");
        factory.setPassword("123.abc");
        factory.setVirtualHost("CommunityApp");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        /*
        消费消息
        1. 队列名称
        2. 消费成功后是否自动应答 true 表示是
        3. 消费者消费的回调函数
        4. 消费者取消消费的回调函数
         */
        DeliverCallback deliverCallback=(var1, var2)->{         //接受消息回调
            System.out.println("消费者消费："+new String(var2.getBody()));
        };
        CancelCallback cancelCallback=(var1)->{                 //取消消息回调
            System.out.println("消费者取消");
        };
        channel.basicConsume("TestQueue1",true,deliverCallback,cancelCallback);
    }

    @Test
    public void dateTest(){

    }
}
