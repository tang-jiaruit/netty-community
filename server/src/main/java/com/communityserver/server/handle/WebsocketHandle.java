package com.communityserver.server.handle;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.community.common.util.*;
import com.communityserver.server.msg.ChatMsg;
import com.communityserver.server.msg.HeartMsg;
import com.communityserver.server.msg.MsgConstance;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.Map;
import static com.community.common.config.rabbitmq.RabbitMqConfig.MESSAGE_DIRECT_EXCHANGE;
import static com.community.common.config.rabbitmq.RabbitMqConfig.USER_ROUTE_KEY;
import static com.community.common.constant.RedisConstant.TOKEN;
import static com.communityserver.server.msg.MsgConstance.*;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/10 20.56
 * @Description：处理websocket请求，这里有几种不同的类型，聊天消息，视频通话，朋友圈，心跳监测
 **/
public class WebsocketHandle extends ChannelInboundHandlerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(WebsocketHandle.class);

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
    }
    /**
     * 用户在建立websocket连接后就马上给服务器发送一条绑定消息，将用户的token发送过来，用于服务器绑定用户信息和channel信息
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        //只处理 websocket 请求
        if (msg instanceof WebSocketFrame){
            handleWebsocketRequest(ctx, (WebSocketFrame) msg);
        }
        //如果都不是就向下传递
        else
            ctx.fireChannelRead(msg);
    }
    private static void handleWebsocketRequest(ChannelHandlerContext ctx, WebSocketFrame frame){
        //判断是否为文本类型的消息
        if (frame instanceof TextWebSocketFrame){

            TextWebSocketFrame textWebSocketFrame = (TextWebSocketFrame) frame;
            String text = textWebSocketFrame.text();
            JSONObject jsonObject = JSONObject.parseObject(text, JSONObject.class);
            Integer type = (Integer) jsonObject.get("type");
//            logger.info(String.valueOf(type));
            //先判断类型 如果是绑定类型，就解析用户的token将用户的t=账号信息和通道进行绑定
            if (type== BIND_MSG){
                // TODO 处理绑定信息
                //先获取token并解析出来
                String t = (String) jsonObject.get("token");
                Map<String, String> token = TokenUtil.getInfoFromToken(t);

                int id = Integer.parseInt(token.get("id"));
                String s = RedisUtil.get(TOKEN + id);
                BaseResponse baseResponse;
                //如果获取的token不为空就表明用户登录成功了
                if (s==null || s.equals("")){
                    baseResponse=ResultUtil.error(5000,"获取用户信息失败请重试");
                }else {
                    baseResponse = ResultUtil.success();
                    //TODO 多线程问题 新开线程监控消息队列 如果现在还没有线程处理用户信息的话就给一个线程
                    HandleMessage.addListener(id, ctx.channel());

                    //TODO 当有用户绑定成功后就需要监听心跳消息，如果发现用户在指定时间内没有消息输入就判断该用户下线，就清理资源
                }
                String bjson = JSON.toJSONString(baseResponse);
                ctx.channel().writeAndFlush(new TextWebSocketFrame(bjson));
                return;
            }
            //如果是心跳信息，就返回一个心跳信息
            else if (type== HEART_MSG){
                //处理心跳信息
                HeartMsg heartMsg = new HeartMsg();
                //TODO 有问题这里为什么前端接收到的是object
                ctx.channel().writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(heartMsg)));
                return;
            }
            else if(type==CHAT_MSG){
                //如果是聊天消息就发送到对应的queue
                try {
                    Integer userId = (Integer) jsonObject.get("sendUserId");
                    Integer friendId = (Integer) jsonObject.get("receiveUserId");
                    String content = (String) jsonObject.get("content");
                    //向队列中发送消息
                    ChatMsg chatMsg = new ChatMsg(content, userId, friendId);
                    RabbitmqUtil.sendMessage(MESSAGE_DIRECT_EXCHANGE,USER_ROUTE_KEY+friendId, chatMsg,chatMsg.getType());
                    return;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //否则就是其它的信息
            System.out.println(jsonObject.get("data"));
            JSONObject data = (JSONObject) jsonObject.get("data");
            System.out.println(data.get("msg"));
            ctx.channel().write(new TextWebSocketFrame("我是服务器"));
            return;
        }

        //判断是否为二进制类型的消息
        if (frame instanceof BinaryWebSocketFrame){

            return;
        }
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent){
            IdleStateEvent idleEvent = (IdleStateEvent) evt;
            if (idleEvent.state() == IdleState.READER_IDLE) {//读
                ctx.channel().close();
            } else if (idleEvent.state() == IdleState.WRITER_IDLE) {//写

            } else if (idleEvent.state() == IdleState.ALL_IDLE) {//全部

            }
        }

        super.userEventTriggered(ctx, evt);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
