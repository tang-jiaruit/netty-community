package com.communityserver.server.handle;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/2 12.08
 * @Description：
 **/
public class ServerHandle extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ServerHandle.class);
    private static ConcurrentMap<String,Channel> map = new ConcurrentHashMap<>();

    //可以使用这个来管理所有连接的channel 使用 channel.id() 来找到特定的channel
    private static ChannelGroup clients = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private static WebSocketServerHandshaker handshaker;
    @Value("${server.address}")
    private static  String host;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if (msg instanceof FullHttpRequest){
            logger.info("http请求");
            ByteBuf byteBuf = Unpooled.copiedBuffer("Hello! 我是Netty服务器 ", CharsetUtil.UTF_8);
            DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, byteBuf);
            //2.1 设置响应头
            response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/html;charset=utf-8");
            response.headers().set(HttpHeaderNames.CONTENT_LENGTH, byteBuf.readableBytes());
            ctx.channel().writeAndFlush(response);
            logger.info("返回请求");
//            handleHttpRequest(ctx, (FullHttpRequest) msg);
        }
        else if (msg instanceof WebSocketFrame){
            logger.info("websocket请求");
        handleWebsocketRequest(ctx, (WebSocketFrame) msg);
        }
        //如果都不是就向下传递
        else
            ctx.fireChannelRead(msg);
    }


    private static void handleHttpRequest(ChannelHandlerContext ctx, FullHttpRequest req){
        //判断http请求解析是否成功
        if (!req.decoderResult().isSuccess() || !("websocket").equals(req.headers().get("Upgrade"))){
            ctx.writeAndFlush(new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST));
        }
        //创建握手处理类
//        String websocketUrl = "ws://"+host+":"+10010+"/websocket";
        String websocketUrl = "ws://127.0.0.1:10010/websocket";

        WebSocketServerHandshakerFactory webSocketServerHandshakerFactory = new WebSocketServerHandshakerFactory(
                websocketUrl,null,false);
        //创建握手器
         handshaker = webSocketServerHandshakerFactory.newHandshaker(req);
        //判断握手是否成功
        if (handshaker==null){
            //握手失败
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        }else {
            handshaker.handshake(ctx.channel(),req);

            //在连接建立后可以将用户信息和对应的channel保存下来，用于区分不同的连接
//            map.put("1", ctx.channel());
        }
    }

    private static void handleWebsocketRequest(ChannelHandlerContext ctx, WebSocketFrame frame){
        //判断是否是关闭链路指令
        if (frame instanceof CloseWebSocketFrame){
            handshaker.close(ctx.channel(),((CloseWebSocketFrame) frame).retain());
            return;
        }

        //判断是否是ping消息
        if (frame instanceof PingWebSocketFrame){
            ctx.channel().write(new PongWebSocketFrame(frame.content().retain()));
            return;
        }

        //判断是否为文本类型的消息
        if (frame instanceof TextWebSocketFrame){

            return;
        }

        //判断是否为二进制类型的消息
        if (frame instanceof BinaryWebSocketFrame){

            return;
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
