package com.communityserver.server.handle;

import com.communityserver.server.util.MessageMonitor;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;


/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/21 16.16
 * @Description：
 **/
public class HandleMessage{

    private static Logger logger = LoggerFactory.getLogger(HandleMessage.class);
    //存放用户与channel的信息
    private static ConcurrentMap<Integer, Future> futureConcurrentMap = new ConcurrentHashMap<>();
    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(20,100,2,
            TimeUnit.SECONDS, new LinkedBlockingQueue(100));
    /**
     * 添加监视对象
     * @param id
     * @param channel
     */
    public static void addListener(int id, Channel channel){

        //先判断是否已经有了线程在监视
        if (futureConcurrentMap.containsKey(id)){
            logger.info("用户为：{} 的channel关闭",id);
            //停止这个任务
            cancelListen(id);
        }
        logger.info("新添用户channel id为：{}",id);
        //新添加任务
        Future<?> future = threadPoolExecutor.submit(new MessageMonitor(id, channel));
        futureConcurrentMap.put(id,future);
    }

    /**
     * 取消任务监视
     * @param id
     */
    public static void cancelListen(int id){
        //TODO 如何在取消时释放资源
        Future future = futureConcurrentMap.get(id);
        future.cancel(true);
    }

}
