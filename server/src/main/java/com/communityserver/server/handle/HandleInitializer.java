package com.communityserver.server.handle;

import com.community.common.util.BaseResponse;

import com.communityserver.server.service.serviceImpl.UserServiceImpl;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/2 11.44
 * @Description：
 **/
public class HandleInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast("http-code", new HttpServerCodec());
        pipeline.addLast("aggregator", new HttpObjectAggregator(65536));
        pipeline.addLast("http-chunked", new ChunkedWriteHandler());
        pipeline.addLast("heartbeat",new IdleStateHandler(10,0,0, TimeUnit.SECONDS));

        pipeline.addLast("websocket", new WebSocketServerProtocolHandler("/ws"));
        pipeline.addLast("httpHandle",new HttpHandle());
        pipeline.addLast("websocketHandle",new WebsocketHandle());
//        pipeline.addLast("handle",new ServerHandle());
    }
}
