package com.communityserver.server.handle;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.community.common.util.BaseResponse;
import com.community.common.util.JsonSerialize;
import com.communityserver.server.controller.ControllerManage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.*;
import io.netty.util.ReferenceCountUtil;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

import static io.netty.handler.codec.http.HttpHeaderNames.*;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/10 20.56
 * @Description：
 **/

public class HttpHandle extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(HttpHandle.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //只处理http请求
        if (msg instanceof FullHttpRequest){
            handleHttpRequest(ctx, (FullHttpRequest) msg);
            ReferenceCountUtil.release(msg); // 释放 ByteBuf 对象
        }
        //如果都不是就向下传递
        else
            ctx.fireChannelRead(msg);
    }


    private static void handleHttpRequest(ChannelHandlerContext ctx, FullHttpRequest req) throws Exception {

//        获取token
        String s = req.headers().get(AUTHORIZATION);

//        if (s==null || s.equals("")){
//            //如果没有token就查看redis是否有token
//            DefaultFullHttpResponse errorResponse = getErrorResponse(4003, "请登录后重试");
//            ctx.channel().writeAndFlush(errorResponse);
//        }
        //如果有token
        //分割路径
        String[] paths = getPath(req.uri());

        //controller返回的内容，先定义
        BaseResponse baseResponse = null;


        //判断是get请求还是Post请求
        //TODO 解析get请求
        if (req.method() == HttpMethod.GET){
            QueryStringDecoder queryStringDecoder = new QueryStringDecoder(req.uri());
            // 解析请求参数
            Map<String, List<String>> parameters = queryStringDecoder.parameters();
            // 将参数转换成 JSON 对象
            String json = JSON.toJSONString(parameters);

        }
        else if (req.method() == HttpMethod.POST){
            //获取post的参数
            String param = req.content().toString(StandardCharsets.UTF_8);
            logger.info(param+"参数");
            JSONObject jsonObject = JSON.parseObject(param);
            logger.info("object  {}",jsonObject);
            baseResponse=ControllerManage.handleReq(paths[0], paths[1], jsonObject);
        }

        byte[] serialize = JsonSerialize.serialize(baseResponse);
            // 在这里执行对 ChannelHandlerContext 的操作
            ctx.channel().writeAndFlush(getResponse("application/json", serialize));
//        ctx.channel().writeAndFlush(getErrorResponse(500,"错误"));

    }

    public static ReentrantLock lock = new ReentrantLock();
    //创建响应对象
    private static DefaultFullHttpResponse getResponse(String contentType, byte[] data){
        lock.lock();
        try {
            //这里的buf不用自己来释放在 HttpEncoder的处理器会释放掉
            ByteBuf buf = Unpooled.directBuffer(data.length);
                buf.writeBytes(data);
                //这里同一返回200  只不过返回的数据是我们自定义的status
                DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,buf);
                //允许跨域访问
                response.headers().set( ACCESS_CONTROL_ALLOW_ORIGIN, "*");
                response.headers().set( ACCESS_CONTROL_ALLOW_HEADERS, "Origin, X-Requested-With, Content-Type, Accept");
                response.headers().set( ACCESS_CONTROL_ALLOW_METHODS, "GET, POST, PUT,DELETE");
                //添加数据信息 如果没有数据信息的话
                response.headers().set(CONTENT_TYPE,contentType);
                if (data==null || data.length<=0){
                    response.headers().set(CONTENT_LENGTH,0);
                }else {
                    response.headers().set(CONTENT_LENGTH,data.length);
                }
                return response;

        }finally {
            lock.unlock();
        }
    }

    private static DefaultFullHttpResponse getErrorResponse(int status,String msg){
        lock.lock();
        try {
            DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST);
            //允许跨域访问
            response.headers().set( ACCESS_CONTROL_ALLOW_ORIGIN, "*");
            response.headers().set( ACCESS_CONTROL_ALLOW_HEADERS, "Origin, X-Requested-With, Content-Type, Accept");
            response.headers().set( ACCESS_CONTROL_ALLOW_METHODS, "GET, POST, PUT,DELETE");
            response.headers().set(CONTENT_LENGTH,0);
            //添加数据信息 如果没有数据信息的话
            response.headers().set(CONTENT_TYPE,"text/html;charset=utf-8");
            return response;
        }
        finally {
            lock.unlock();
        }
    }

    //分割路由路径
    private static String[] getPath(String url){
        String[] split = url.split("/");
        if (split == null || split.length==0){
            return null;
        }
        String[] paths = new String[2];
        paths[0] = split[split.length-2];
        paths[1] = split[split.length-1];
        return   paths;
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
