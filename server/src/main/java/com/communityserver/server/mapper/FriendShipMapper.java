package com.communityserver.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.communityserver.server.pojo.Friendship;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/18 20.04
 * @Description：
 **/
@Mapper
public interface FriendShipMapper extends BaseMapper<Friendship> {
}
