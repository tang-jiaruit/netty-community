package com.communityserver.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.communityserver.server.pojo.UserInfo;
import org.apache.ibatis.annotations.Mapper;
/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/14 11.24
 * @Description：
 **/
@Mapper
public interface UserMapper extends BaseMapper<UserInfo> {
}
