package com.communityserver.server.controller;

import com.community.common.util.BaseResponse;
import com.communityserver.server.controller.httpcontroller.UserController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/14 16.35
 * @Description：
 **/

public class ControllerManage {

     private static final UserController userController = new UserController();
     //根据路径选择要使用的controller
     public static BaseResponse handleReq(String path, String method,Object parm) throws Exception {
          switch (path){
               case "user":
                         return todo(userController,method,parm);
          }
          return null;
     }

     //执行具体的方法
     private static <T> BaseResponse todo(T t, String method, Object parm) throws Exception {
          Class<?> aClass = t.getClass();
          Method method1 = aClass.getMethod(method, parm.getClass());
          Object invoke = method1.invoke(t, parm);
          BaseResponse invoke1 = (BaseResponse) invoke;
          return invoke1;
     }


}
