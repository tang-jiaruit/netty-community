package com.communityserver.server.controller.httpcontroller;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson2.JSONObject;
import com.community.common.util.BaseResponse;
import com.communityserver.server.service.UserService;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/14 11.04
 * @Description：用户请求处理，比如查询用户信息，修改信息等
 **/

public class UserController{
    private UserService userService;
    public UserController(){
        this.userService =SpringUtil.getBean(UserService.class);
    }
    //查找用户
    public BaseResponse selectUser(JSONObject jsonObject){
        return userService.selectUser(jsonObject);
    }

    //根据id查询用户
    public BaseResponse selectUserById(JSONObject jsonObject){
        return userService.selectUserById(jsonObject);
    }
    //添加好友 前端传入userId，friendId
    public BaseResponse friendAdd(JSONObject jsonObject){
        return userService.friendAdd(jsonObject);
    }

    //处理好友添加请求 前端传入好友申请id和用户接收或者拒绝
    public BaseResponse handleFriendAdd(JSONObject jsonObject){
        return userService.handleFriendAdd(jsonObject);
    }

    //获取所有的好友信息
    public BaseResponse getFriends(JSONObject jsonObject){
        return userService.getFriends(jsonObject);
    }
}
