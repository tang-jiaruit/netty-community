package com.communityserver.server.registry;

import com.community.common.pojo.ServerInfo;
import com.community.common.util.JsonSerialize;
import com.community.common.zookeeper.CuratorClient;

import static com.community.common.config.ZookeeperConfig.ZK_REGISTRY_PATH;

/**
 * @Author： tjr
 * @CreateTime：2023-10-2023/10/31 15.30
 * @Description：服务注册 将自己的地址端口，服务名注册到zookeeper中
 **/
public class ServerRegistry {

    private CuratorClient curatorClient;
    //这个构造用于连接指定的zookeeper
    public ServerRegistry(String connectAddress){
        this.curatorClient = new CuratorClient(connectAddress);
    }
    public ServerRegistry(){
        this.curatorClient = new CuratorClient();
    }

    public void registryServer(String host, int port , String serverName){
        //在zookeeper上面
        ServerInfo serverInfo = new ServerInfo(host, port, serverName);
        byte[] serialize = JsonSerialize.serialize(serverInfo);

        try {
            String znodePath = ZK_REGISTRY_PATH+"/"+serverInfo.getServerName();

            String pathData = curatorClient.createPathData(znodePath, serialize);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
