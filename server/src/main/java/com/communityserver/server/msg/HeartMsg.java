package com.communityserver.server.msg;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

import static com.communityserver.server.msg.MsgConstance.HEART_MSG;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/27 16.32
 * @Description：
 **/
//这个不需要向数据库中写
@Data
public class HeartMsg implements Serializable {
    //消息类型
    private int type;
    //消息内容 这里是字节数组，一般就是字符串消息
    private byte[] content;
    //消息发送时间
    private Date sendDate;
    public HeartMsg(){
        this.type = HEART_MSG;
        this.content = "heart".getBytes();
        this.sendDate = new Timestamp(Instant.now().getEpochSecond());
    }

    @Override
    public String toString() {
        return this.type+"  数据"+this.content+"  时间"+this.sendDate;
    }
}
