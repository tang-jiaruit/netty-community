package com.communityserver.server.msg;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

import static com.communityserver.server.msg.MsgConstance.BIND_MSG;

/**
 * @Author： tjr
 * @CreateTime：2023-12-2023/12/1 11.02
 * @Description：
 **/
@AllArgsConstructor
@Data
public class BindingMsg {
    private int type;
    private String content;
    private Date date;
    public BindingMsg(){
        this.type = BIND_MSG;
        this.content =  "Bind success";
        this.date = new Date();
    }
}
