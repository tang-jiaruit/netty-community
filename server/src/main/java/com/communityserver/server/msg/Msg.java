package com.communityserver.server.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author： tjr
 * @CreateTime：2023-12-2023/12/1 11.58
 * @Description：
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Msg implements Serializable {
    //消息类型
    protected int type;
    //消息内容 这里是字节数组，一般就是字符串消息
    protected String content;
    //消息发送方
    protected int sendUserId;
    //消息接收方
    protected int receiveUserId;
    //消息发送时间
    protected Date sendDate;

    @Override
    public String toString() {
        return this.type+"  数据"+this.content+"  时间"+this.sendDate + "  "+this.sendUserId + "内容："+content;
    }
}
