package com.communityserver.server.msg;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

import static com.communityserver.server.msg.MsgConstance.FRIENDADD_MSG;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/27 16.33
 * @Description：
 **/
//这个不需要向数据库中写
@AllArgsConstructor
@Data
public class FriendAddMsg extends Msg implements Serializable {
    public FriendAddMsg(int sendUserId, int receiveUserId){
        this.type = FRIENDADD_MSG;
        this.content = "添加好友请求";
        this.sendUserId = sendUserId;
        this.receiveUserId = receiveUserId;
        this.sendDate = new Date();
    }
    /**
     * 现在有三种类型的消息
     * 心跳消息     不需要保存到数据库
     * 好友申请消息 不需要保存到数据库
     * 聊天消息     需要保存到数据库
     *
     */
}
