package com.communityserver.server.msg;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

import static com.communityserver.server.msg.MsgConstance.CHAT_MSG;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/21 15.31
 * @Description
 **/
@AllArgsConstructor
@Data
public class ChatMsg extends Msg implements Serializable {

    public ChatMsg(String content,int sendUserId, int receiveUserId){
        this.type = CHAT_MSG;
        this.content = content;
        this.sendUserId = sendUserId;
        this.receiveUserId = receiveUserId;
        this.sendDate = new Date();
    }
}
