package com.communityserver.server.msg;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/27 16.33
 * @Description：
 **/
public interface MsgConstance {
    //绑定消息
    int BIND_MSG = -1;
    //心跳消息
    int HEART_MSG = 0;
    //聊天消息
    int CHAT_MSG = 1;
    //用户添加消息
    int FRIENDADD_MSG = 2;
    //朋友圈消息
}
