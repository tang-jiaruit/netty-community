package com.communityserver.server.dto.user;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/14 11.47
 * @Description：用户信息视图
 **/
@Data
public class UserInfoVo {
    private int userId;
    private String userName;
    private String level;
    private String userAccount;
    private String headPicture;
}
