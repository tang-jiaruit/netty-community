package com.communityserver.server.util;


import java.util.concurrent.*;

/**
 * @Author： tjr
 * @CreateTime：2023-12-2023/12/3 10.54
 * @Description：
 **/
public class Ttes implements RunnableFuture {

    ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);    @Override
    public void run() {
        scheduledExecutorService.submit(new Ttes());
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public Object get() throws InterruptedException, ExecutionException {
        return null;
    }

    @Override
    public Object get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return null;
    }
}
