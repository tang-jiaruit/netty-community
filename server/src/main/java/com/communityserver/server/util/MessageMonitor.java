package com.communityserver.server.util;

import com.alibaba.fastjson2.JSON;
import com.community.common.util.JsonSerialize;
import com.communityserver.server.msg.ChatMsg;
import com.communityserver.server.msg.FriendAddMsg;
import com.communityserver.server.msg.Msg;
import com.rabbitmq.client.*;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static com.community.common.config.rabbitmq.RabbitMqConfig.*;
import static com.community.common.config.rabbitmq.RabbitMqConnectionCore.getChannel;
import static com.community.common.util.RabbitmqUtil.sendMessage;
import static com.communityserver.server.msg.MsgConstance.CHAT_MSG;
import static com.communityserver.server.msg.MsgConstance.FRIENDADD_MSG;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/26 20.17
 * @Description：
 **/
public class MessageMonitor implements Runnable{

    private static Logger logger = LoggerFactory.getLogger(MessageMonitor.class);
    private Channel nettyChannel;

    //关闭资源的标记 如果要关闭资源就
    private static boolean clearFlag = false;

    com.rabbitmq.client.Channel rabbitmqChannel = null;
    private int id;
    public MessageMonitor(int id,Channel nettyChannel){
        this.nettyChannel  = nettyChannel;
        this.id = id;
    }

    @Override
    public void run() {
        try {
            //TODO 监视rabbitmq
            //获取一个channel
            rabbitmqChannel = getChannel(false);
            //取消自动应答    这个并不是阻塞，而是将消息处理交给 MyConsumer 所以 MyConsumer 里面的内容才是阻塞方法
            rabbitmqChannel.basicConsume(Msg_QUEUE_NAME + id, false, new MyConsumer(rabbitmqChannel));
        } catch (IOException e) {
            e.printStackTrace();
            clearRe();
        }
    }
    /**
     * 关闭资源
     */
    private void clearRe(){
        try {
            logger.info("id为：{} 的用户，关闭连接",id);
            nettyChannel.close();
            rabbitmqChannel.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }
    class MyConsumer extends DefaultConsumer{
        private com.rabbitmq.client.Channel rabbitmqChannel;
        public MyConsumer(com.rabbitmq.client.Channel channel) {
            super(channel);
            this.rabbitmqChannel = channel;
        }
        @Override
        public void handleCancelOk(String consumerTag) {
            logger.info("消费者取消订阅：{}",consumerTag);
        }

        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
            //TODO 处理body
            try {
                Map<String, Object> headers = properties.getHeaders();
                //TODO 这里不应该这样判断 应该先判断是不是Msg消息，也可能是其它服务的消息比如支付等，后面需要再完善
                if (headers==null){
                    return;
                }
                Integer msg_type = (Integer) headers.get("MSG_TYPE");
                if (msg_type==null){
                    logger.info("header为{}",properties);
                    return;
                }
                Msg msg = null;
                if (msg_type== CHAT_MSG){
                    msg = JsonSerialize.deserialize(body, ChatMsg.class);
                }else if (msg_type == FRIENDADD_MSG){
                    msg = JsonSerialize.deserialize(body, FriendAddMsg.class);
                }
                logger.info("id为 {} 的用户接收消息：{}",id,msg);
                nettyChannel.writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(msg)));
             /*
                1. 消息的标记 tag
                2. 是否批量应答 false：不
                 */
                rabbitmqChannel.basicAck(envelope.getDeliveryTag(),false);

            }catch (Exception e){
                getChannel().basicNack(envelope.getDeliveryTag(),false,true);
                e.printStackTrace();
            }
        }

    }
}
