package com.communityserver.server.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/18 20.00
 * @Description：
 **/
@TableName("friendship")
@Data
public class Friendship {

    @TableId("FriendshipID")
    private int FriendshipID;
    @TableField("UserID")
    private int UserID;
    @TableField("FriendID")
    private int FriendID;
    @TableField("Status")   //'pending','accepted','refuse'
    private String Status;
    @TableField("Timestamp")
    private Date Timestamp;
}
