package com.communityserver.server.pojo.annotation;

import java.lang.annotation.*;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/14 11.11
 * @Description：用于标记是否需要登录后才能调用方法
 **/
//用于方法，或者类
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginNeed {

}
