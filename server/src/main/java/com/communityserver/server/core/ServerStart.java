package com.communityserver.server.core;

import com.communityserver.server.registry.ServerRegistry;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;


/**
 * @Author： tjr
 * @CreateTime：2023-10-2023/10/31 16.03
 * @Description：服务启动类，在springboot项目启动后就会运行这个代码 ApplicationRunner
 **/
@Component
public class ServerStart implements ApplicationRunner {

    public static Logger logger = LoggerFactory.getLogger(ServerStart.class);

    private static ServerRegistry registry = new ServerRegistry();

    @Value("${netty.ip}")
    String serverHost;
    @Value("${netty.port}")
    int port;
    @Value("${server.serverName}")
    String serverName;

    @Override
    public void run(ApplicationArguments args) throws Exception {
//
//        //TODO 这里是否需要同步，及保证注册成功后在启动netty
        registry.registryServer(serverHost, port, serverName);
        TimeUnit.SECONDS.sleep(1);
        //运行netty
        new Thread(new ServerNettyStart(serverHost, port)).start();
        logger.info("ok");

    }
}
