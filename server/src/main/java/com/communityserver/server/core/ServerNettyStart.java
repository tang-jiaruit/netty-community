package com.communityserver.server.core;

import com.communityserver.server.handle.HandleInitializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;


/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/2 11.38
 * @Description：
 **/
public class ServerNettyStart implements Runnable{
    private static final  Logger logger = LoggerFactory.getLogger(ServerNettyStart.class);
    public ServerNettyStart(String host, int port){
        this.ip = host;
        this.port = port;
    }
    private  String ip;
    private int port;
    EventLoopGroup boss = new NioEventLoopGroup(4);
    EventLoopGroup worker = new NioEventLoopGroup(16);
    @Override
    public void run() {
        try {
            nettyServerStart();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void nettyServerStart() throws InterruptedException {
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap()
                    .group(boss, worker)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG,1024)
                    .childOption(ChannelOption.TCP_NODELAY,true)
                    .childHandler(new HandleInitializer());

            ChannelFuture sync = serverBootstrap.bind(new InetSocketAddress(ip, port)).sync();
            sync.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture channelFuture) throws Exception {
                    logger.info("netty 服务启动成功，运行地址：{}:{}",ip,port);
                }
            });

            sync.channel().closeFuture().sync();
        }finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }



    }
}
