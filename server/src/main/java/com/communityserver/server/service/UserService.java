package com.communityserver.server.service;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.community.common.util.BaseResponse;
import com.communityserver.server.pojo.UserInfo;
import org.springframework.stereotype.Service;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/14 11.24
 * @Description：
 **/
public interface UserService extends IService<UserInfo>{

    //查询用户表 根据用户账号
    BaseResponse selectUser(JSONObject jsonObject);

    //添加好友 发送好友请求
    BaseResponse friendAdd(JSONObject jsonObject);

    BaseResponse handleFriendAdd(JSONObject jsonObject);

    BaseResponse getFriends(JSONObject jsonObject);

    BaseResponse selectUserById(JSONObject jsonObject);

    //同意好友请求

}
