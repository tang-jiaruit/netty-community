package com.communityserver.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.communityserver.server.pojo.Friendship;

import java.util.List;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/18 20.05
 * @Description：
 **/
public interface FriendShipService extends IService<Friendship> {

    List<Integer> getUserFrineds(int id);

}
