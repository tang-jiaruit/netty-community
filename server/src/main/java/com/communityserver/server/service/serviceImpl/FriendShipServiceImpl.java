package com.communityserver.server.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.communityserver.server.mapper.FriendShipMapper;
import com.communityserver.server.pojo.Friendship;
import com.communityserver.server.service.FriendShipService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/18 20.06
 * @Description：
 **/
@Service
public class FriendShipServiceImpl extends ServiceImpl<FriendShipMapper, Friendship> implements FriendShipService {


    @Override
    public List<Integer> getUserFrineds(int id) {

        QueryWrapper<Friendship> wrapper = new QueryWrapper<>();
        wrapper.select("UserId","FriendID").eq("FriendID",id).or().eq("UserId",id);
        List<Friendship> list = list(wrapper);
//        QueryWrapper<Friendship> wrapper1 = new QueryWrapper<>();
//        wrapper1.select("FriendID").eq("UserId",id);
//        List<Friendship> list1 = list(wrapper1);
        return getId(new ArrayList<Integer>(), list,id);
    }

    public List<Integer> getId(List<Integer> result, List<Friendship> list,int id){
        for (Friendship friendship : list) {
            System.out.println(friendship);
            if (friendship.getFriendID()!=id){
                result.add(friendship.getFriendID());
            }else {
                result.add(friendship.getUserID());
            }
        }
        return result;
    }
}
