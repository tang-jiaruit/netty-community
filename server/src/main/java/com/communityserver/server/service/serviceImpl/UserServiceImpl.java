package com.communityserver.server.service.serviceImpl;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.community.common.util.BaseResponse;
import com.community.common.util.ResultUtil;
import com.communityserver.server.dto.user.UserInfoVo;
import com.communityserver.server.mapper.UserMapper;
import com.communityserver.server.msg.FriendAddMsg;
import com.communityserver.server.pojo.Friendship;
import com.communityserver.server.pojo.UserInfo;
import com.communityserver.server.service.FriendShipService;
import com.communityserver.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import static com.community.common.config.rabbitmq.RabbitMqConfig.MESSAGE_DIRECT_EXCHANGE;
import static com.community.common.config.rabbitmq.RabbitMqConfig.USER_ROUTE_KEY;
import static com.community.common.util.RabbitmqUtil.sendMessage;
import static com.communityserver.server.msg.MsgConstance.FRIENDADD_MSG;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/14 11.24
 * @Description：
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserInfo> implements UserService{
    @Autowired
    FriendShipService friendShipService;
    @Value("${picture_path}")
    String PICTURE_PATH;
    @Override
    public BaseResponse selectUser(JSONObject jsonObject) {
        String userAccount = (String) jsonObject.get("userAccount");
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("userAccount",userAccount);
        UserInfo user = getOne(wrapper);
        if (user==null){
            return ResultUtil.error(4000,"查询失败，没有相关信息");
        }
        return ResultUtil.success(getUserInfoVo(user));

    }

    @Override
    public BaseResponse friendAdd(JSONObject jsonObject) {
        //在数据库中写入信息 userId是自己的id  friendId是添加的好友的id
        Friendship friendship = new Friendship();
        Integer userId = (Integer) jsonObject.get("userId");
        Integer friendId = (Integer) jsonObject.get("friendId");
        friendship.setUserID(userId);
        friendship.setUserID(friendId);
        friendship.setStatus("pending");
        friendship.setTimestamp(new Timestamp(Instant.now().getEpochSecond()));

        boolean save = friendShipService.save(friendship);
        if (save){
            //TODO 如果添加成功就向对方的队列中添加一条消息
            QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("id",friendship.getFriendID());
            UserInfo userInfo = baseMapper.selectOne(wrapper);
            //拿到好友的账号信息 并给他的消息队列发送消息
            String userAccount = userInfo.getUserAccount();
            try {
                FriendAddMsg friendAddMsg = new FriendAddMsg(userId, friendId);
                sendMessage(MESSAGE_DIRECT_EXCHANGE,USER_ROUTE_KEY+userAccount,friendAddMsg,friendAddMsg.getType());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ResultUtil.success();
        }

        return ResultUtil.error(5001,"添加失败请重试");
    }

    @Override
    public BaseResponse handleFriendAdd(JSONObject jsonObject) {
        String status = (String) jsonObject.get("status");
        int friendShipId = (int) jsonObject.get("friendShipId");

        UpdateWrapper<Friendship> wrapper = new UpdateWrapper<>();
        wrapper.eq("FriendshipID",friendShipId);
        wrapper.set("Status",status);
        boolean update = friendShipService.update(wrapper);
        if (update) {
            return ResultUtil.success();
        }
        return ResultUtil.error(5001,"处理失败，请重试");
    }

    @Override
    public BaseResponse getFriends(JSONObject jsonObject) {

        int id = (int) jsonObject.get("id");

//        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
//        // 添加自定义的排序语句
//        String customOrderBy = "CONVERT(username USING gbk) COLLATE gbk_chinese_ci";
//        wrapper.orderBy(true, true, customOrderBy);

        //先根据id查询用户的好友的id信息
        List<Integer> ids = friendShipService.getUserFrineds(id);
        //再根据这些id查询用户的基本信息
        List<UserInfo> userInfos = baseMapper.selectBatchIds(ids);
        List<UserInfoVo> collect = userInfos.stream().map(this::getUserInfoVo).collect(Collectors.toList());
        return ResultUtil.success(collect);
    }

    @Override
    public BaseResponse selectUserById(JSONObject jsonObject) {
        Integer id = Integer.parseInt((String) jsonObject.get("id"));
        UserInfo byId = getById(id);
        if (byId==null){
            return ResultUtil.error(4000,"查询失败，没有相关信息");
        }
        UserInfoVo userInfoVo = getUserInfoVo(byId);
        return ResultUtil.success(userInfoVo);
    }

    public  UserInfoVo getUserInfoVo(UserInfo userInfo){
        UserInfoVo userInfoVo = new UserInfoVo();
        userInfoVo.setUserId(userInfo.getId());
        userInfoVo.setUserName(userInfo.getUserName());
        userInfoVo.setUserAccount(userInfo.getUserAccount());
        userInfoVo.setLevel(userInfo.getLevel());
        String s = PICTURE_PATH + userInfo.getHeadPicture();
        userInfoVo.setHeadPicture(s);
        return userInfoVo;
    }
}
