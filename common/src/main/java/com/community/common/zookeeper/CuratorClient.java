package com.community.common.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.Watcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.community.common.config.ZookeeperConfig.*;

/**
 * @Author： tjr
 * @CreateTime：2023-10-2023/10/31 15.15
 * @Description：用于连接zookeeper，用于对节点增删改查
 **/
public class CuratorClient {

    public static Logger logger = LoggerFactory.getLogger(CuratorClient.class);
    private CuratorFramework client;
    public CuratorClient(String connectString, String namespace, int sessionTimeout, int connectionTimeout) {
        client = CuratorFrameworkFactory.builder().namespace(namespace).connectString(connectString)
                .sessionTimeoutMs(sessionTimeout).connectionTimeoutMs(connectionTimeout)
                .retryPolicy(new ExponentialBackoffRetry(1000, 10))
                .build();
        //添加连接监听对象
        client.getConnectionStateListenable().addListener(new MyListener());
        client.start();
    }

    public CuratorClient(String connectAddress,int timeout){
        this(connectAddress, ZK_NAMESPACE, timeout, timeout);
    }
    public CuratorClient(String connectAddress){
        this(connectAddress,ZK_NAMESPACE,ZK_SESSION_TIME,ZK_CONNECTION_TIME);
    }
    public CuratorClient(){
        this(ZK_CONNECT_ADDRESS,ZK_NAMESPACE,ZK_SESSION_TIME,ZK_CONNECTION_TIME);
    }

    public CuratorFramework getClient() {
        return client;
    }

    public void addConnectionStateListener(ConnectionStateListener connectionStateListener) {
        client.getConnectionStateListenable().addListener(connectionStateListener);
    }

    //在断开连接时会将节点删除
    public String createPathData(String path, byte[] data) throws Exception {
        logger.info("开始注册服务：{}",client.getNamespace());
        return client.create().creatingParentsIfNeeded()
                .withMode(CreateMode.EPHEMERAL_SEQUENTIAL)
                .forPath(path, data);
    }

    public void updatePathData(String path, byte[] data) throws Exception {
        client.setData().forPath(path, data);
    }

    public void deletePath(String path) throws Exception {
        client.delete().forPath(path);
    }

    public void watchNode(String path, Watcher watcher) throws Exception {
        client.getData().usingWatcher(watcher).forPath(path);
    }

    public byte[] getData(String path) throws Exception {
        return client.getData().forPath(path);
    }

    public List<String> getChildren(String path) throws Exception {
        return client.getChildren().forPath(path);
    }

    public void watchTreeNode(String path, TreeCacheListener listener) {
        TreeCache treeCache = new TreeCache(client, path);
        treeCache.getListenable().addListener(listener);
    }

    public void watchPathChildrenNode(String path, PathChildrenCacheListener listener) throws Exception {
        PathChildrenCache pathChildrenCache = new PathChildrenCache(client, path, true);
        //BUILD_INITIAL_CACHE 代表使用同步的方式进行缓存初始化。
        pathChildrenCache.start(PathChildrenCache.StartMode.BUILD_INITIAL_CACHE);
        pathChildrenCache.getListenable().addListener(listener);
    }

    class MyListener implements ConnectionStateListener{

        @Override
        public void stateChanged(CuratorFramework client, ConnectionState newState) {

            if (newState == ConnectionState.LOST){
                logger.info("与zookeeper失去连接");
                client.start();
            }else if (newState == ConnectionState.CONNECTED){
                logger.info("zookeeper连接成功");
            }

        }
    }
    public void close() {
        client.close();
    }

}