package com.community.common.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/3 20.17
 * @Description：
 **/
public class TokenUtil {
//    https://blog.csdn.net/qq_45332753/article/details/113833777
    private static final String SECRET = "YXL_2198176247_tjr_6666666666666666";
    private static SecretKey key = Keys.hmacShaKeyFor(SECRET.getBytes(StandardCharsets.UTF_8));
    //传入用户账号和权限
    /**
     *这里还可以将更多的信息保存在token中，可以使用泛型，现在就只先保存这两个信息
     */
    public static String getToken(String userAccount, String level, String id){

        return Jwts.builder()
                .setSubject(userAccount)
                .signWith(key,SignatureAlgorithm.HS256)
                .claim("level",level)
                .claim("id",id)
                .compact();
    }
    //从token中获取用户账号信息，和权限
    public static Map<String, String> getInfoFromToken(String token) {
        Map<String, String> map = new HashMap<>();
        Claims claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
        map.put("userAccount",claims.getSubject());
        map.put("level", claims.get("level",String.class));
        map.put("id",claims.get("id",String.class));
        return map;
    }

}
