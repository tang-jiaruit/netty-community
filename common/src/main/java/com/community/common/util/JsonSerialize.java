package com.community.common.util;

import com.alibaba.fastjson2.JSON;
/**
 * @Author： tjr
 * @CreateTime：2023-10-2023/10/31 17.15
 * @Description：
 **/
public class JsonSerialize {
    public static  <T>  byte[] serialize(T object){
        return JSON.toJSONBytes(object);
    }

    public static <T> T deserialize(byte [] bytes, Class<T> obj){
        return JSON.parseObject(bytes, obj);
    }
}
