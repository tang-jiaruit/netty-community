package com.community.common.util;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;

import java.io.IOException;
import java.util.HashMap;

import static com.community.common.config.rabbitmq.RabbitMqConfig.MESSAGE_DIRECT_EXCHANGE;
import static com.community.common.config.rabbitmq.RabbitMqConfig.USER_ROUTE_KEY;
import static com.community.common.config.rabbitmq.RabbitMqConnectionCore.getAdmin;
import static com.community.common.config.rabbitmq.RabbitMqConnectionCore.getChannel;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/26 17.41
 * @Description：
 **/
public class RabbitmqUtil {

    private static Logger logger = LoggerFactory.getLogger(RabbitmqUtil.class);
    private static RabbitAdmin rabbitAdmin;
    static {
        rabbitAdmin = getAdmin();
    }
    /**
     * 声名交换机
     */
    public static void createDirectExchange(String exchangeName, boolean durable, boolean autoAsk){
        DirectExchange directExchange = new DirectExchange(exchangeName, durable, autoAsk);
        rabbitAdmin.declareExchange(directExchange);
    }
    /**
     * 删除交换机
     */
    public static void deleteExchange(String exchangeName){
        rabbitAdmin.deleteExchange(exchangeName);
    }
    /**
     * 声名队列
     */
    public static void createQueue(String queueName, boolean durable,  boolean exclusive, boolean autoDelete){
        Queue queue = new Queue(queueName, durable, exclusive, autoDelete);
        rabbitAdmin.declareQueue(queue);
    }

    /**
     * 删除队列
     */
    public static void deleteQueue(String queueName){
        rabbitAdmin.deleteQueue(queueName);
    }
    /**
     * 绑定交换机和队列
     */
    public static void bind(String queueName, String exchangeName, String routingKey){
        rabbitAdmin.declareBinding(new Binding(queueName, Binding.DestinationType.QUEUE,exchangeName,routingKey,null));
    }

    /**
     * 发送消息
     */
    public static void sendMessage( String exchange, String routingKey,Object msg,int type) throws IOException {
        Channel channel = getChannel(false);
        channel.confirmSelect();
        try {
            if (channel.waitForConfirms()) {
                logger.info("发送成功");
            }
            else {
                logger.info("发送失败");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        byte[] serialize = JsonSerialize.serialize(msg);
        HashMap<String, Object> headers = new HashMap<>();
        headers.put("MSG_TYPE",type);
        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder().headers(headers).build();
//        logger.info("prop {}",properties);
        //TODO 第三个参数添加类型信息
        channel.basicPublish(exchange,routingKey,properties,serialize);
//        channel.basicPublish(exchange,routingKey,true,null,JsonSerialize.serialize(msg));

    }

}
