package com.community.common.util;
import com.community.common.constant.ReqCode;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 17.05
 * @Description：
 **/
@Data
public class BaseResponse implements Serializable {

    private int code;
    private String msg;
    private Object data;
    private String contentType;

    public BaseResponse(int code, String msg, Object data){
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.contentType = "application/json";
    }

    public BaseResponse(int code, String msg,String contentType, Object data){
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.contentType = "application/json";
    }
    public BaseResponse(int code, String msg){
        this(code, msg ,null);
    }
    public BaseResponse(ReqCode reqCode){
        this(reqCode.getCode(),reqCode.getMsg());
    }

}
