package com.community.common.util;

import com.community.common.constant.ReqCode;
import lombok.Data;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 17.23
 * @Description：
 **/
public class ResultUtil {

    /***
     * 成功，返回数据
     */
    public static BaseResponse success(Object data){
        return new BaseResponse(0,"ok",data);
    }

    public static BaseResponse success(String msg){
        return new BaseResponse(0,msg,null);
    }

    public static BaseResponse success(){
        return new BaseResponse(0,"ok",null);
    }
    /**
     * 失败
     */
    public static BaseResponse error(int code, String msg){
        return new BaseResponse(code,msg);
    }
    public static BaseResponse error(ReqCode reqCode){
        return new BaseResponse(reqCode);
    }

}

