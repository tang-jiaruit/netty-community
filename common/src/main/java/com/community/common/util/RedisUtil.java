package com.community.common.util;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson2.JSON;
import com.community.common.pojo.ServerInfo;
import org.springframework.aop.target.LazyInitTargetSource;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/3 19.51
 * @Description：
 **/
public class RedisUtil {

//    private StringRedisTemplate stringRedisTemplate;
//
//    public StringRedisTemplate getStringRedisTemplate() {
//        return stringRedisTemplate;
//    }
private static StringRedisTemplate stringRedisTemplate;

    static {
        RedisUtil.stringRedisTemplate = SpringUtil.getBean(StringRedisTemplate.class);
    }
    public void setStringRedisTemplate(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    public RedisUtil(StringRedisTemplate stringRedisTemplate){
        System.out.println(stringRedisTemplate);
        this.stringRedisTemplate = stringRedisTemplate;
    }

    //set
    public static void set(String key, String value){
        stringRedisTemplate.opsForValue().set(key,value);
    }
    //set 并传入过期时间
    public static void setExpire(String key, String value, long expire,TimeUnit timeUnit){
        stringRedisTemplate.opsForValue().set(key,value,expire,timeUnit);
    }
    public static void setExpire(String key, String value, long expire){
        stringRedisTemplate.opsForValue().set(key,value,expire,TimeUnit.SECONDS);
    }
    //保存对象
    public static void setObject(String key,Object obj){
        String toJSONString = JSON.toJSONString(obj);
        set(key,toJSONString);
    }
    //保存对象指定过期时间
    public static void setObjectExpire(String key, Object obj, long expire){
        String toJSONString = JSON.toJSONString(obj);
        setExpire(key, toJSONString, expire);
    }
    //get
    public static String get(String key){
        return stringRedisTemplate.opsForValue().get(key);
    }

    //获取对象
    public static  <T> T getObject(String key, Class<T> tClass){
        String value = get(key);
        return JSON.parseObject(value, tClass);
    }

    //修改过期时间
    public static void updateExpire(String key, long time, TimeUnit timeUnit){
        stringRedisTemplate.expire(key, time,timeUnit);
    }

    //模糊查询
    public static Set<String> getKeys(String prefix){
        return stringRedisTemplate.keys(prefix);
    }

    //得到所有的信息
    public static List<String> getLike(String prefix){
        List<String> list = new ArrayList<>();
        Set<String> keys = getKeys(prefix);
        for (String key : keys) {
            list.add(get(key));
        }
        return list;
    }

    //setNX
    public static void setNx(String key, String value){

        stringRedisTemplate.opsForValue().setIfAbsent(key, value);
    }

    //原子加
    public static Long getAdnIncrement(String key){
       return stringRedisTemplate.opsForValue().increment(key);
    }
}
