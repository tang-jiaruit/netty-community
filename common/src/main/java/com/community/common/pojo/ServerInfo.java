package com.community.common.pojo;

import lombok.Data;

import java.util.Objects;

/**
 * @Author： tjr
 * @CreateTime：2023-10-2023/10/31 16.11
 * @Description：
 **/
@Data
public class ServerInfo {
    //服务地址
    public String host;
    //服务端口
    public int port;
    //服务名
    public String serverName;

    public ServerInfo(String host, int port, String serverName){
        this.host = host;
        this.port = port;
        this.serverName = serverName;
    }
    //注册到zookeeper上的节点名就时用这个 哈希码
    @Override
    public int hashCode() {
        return Objects.hash(host, port, serverName.hashCode());
    }

    @Override
    public String toString(){

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("host:"+getHost());
        stringBuilder.append("  port:"+getPort());
        stringBuilder.append("  serverName:"+getServerName());
        return stringBuilder.toString();
    }
}
