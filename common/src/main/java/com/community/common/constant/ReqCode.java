package com.community.common.constant;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 16.58
 * @Description：
 **/
public enum ReqCode {

    SUCCESS(0,"ok"),
    PARAMS_ERROR(40000, "请求参数错误"),
    NOT_LOGIN_ERROR(40100, "未登录"),
    NO_AUTH_ERROR(40101, "无权限"),
    NOT_FOUND_ERROR(40400, "请求数据不存在"),
    FORBIDDEN_ERROR(40300, "禁止访问"),
    SYSTEM_ERROR(50000, "系统内部异常"),
    OPERATION_ERROR(50001, "操作失败");
    ;

    //状态信息
    private String msg;
    //状态码
    private int code;

    private ReqCode(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
