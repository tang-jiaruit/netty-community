package com.community.common.constant;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 11.31
 * @Description：
 **/
public interface RedisConstant {
    //这个表示在redis中存储的唯一的账号信息，保存的长度为6个数字的长度，剩余4个数字用随机数生成，然后拼接为用户账号
    String USERACCOUNTPREFIX = "cmi:accountPrefix";
    //用户的验证码
    String USER_CODE = "cmi:code:";
    //用户token
    String TOKEN = "cmi:token:";
    //token过期时间
    Long TOKEN_EXPIRE = 5*24L;
    //验证码过期时间 5分钟
    Long CODE_EXPIRE = 5L;
}
