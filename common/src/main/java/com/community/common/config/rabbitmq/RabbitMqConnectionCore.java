package com.community.common.config.rabbitmq;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import static com.community.common.config.rabbitmq.RabbitMqConfig.*;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/18 10.24
 * @Description：
 **/
public class RabbitMqConnectionCore {


    private static ConnectionFactory connectionFactory;
    private static Connection defaultConnection;
    static {
        connectionFactory = createFactory();
        defaultConnection = getConnection();
    }
    private static ConnectionFactory createFactory(){
        // 创建CachingConnectionFactory实例
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        // 设置RabbitMQ服务器的连接信息
        connectionFactory.setHost(RABBITMQ_HOST);
        connectionFactory.setPort(RABBITMQ_PORT);
        connectionFactory.setUsername(RABBITMQ_USERNAME);
        connectionFactory.setPassword(RABBITMQ_PASSWORD);
        connectionFactory.setVirtualHost(RABBITMQ_VIRTUALHOST);
        connectionFactory.setConnectionTimeout(RABBITMQ_TIMEOUT);
//        connectionFactory.setPublisherReturns(true); // 启用消息返回机制
//        connectionFactory.setPublisherConfirms(true); // 启用消息确认机制
        //最大连接数
//        connectionFactory.setRequestedChannelMax(2024);
        return connectionFactory;
    }

    public static RabbitAdmin getAdmin(){
        RabbitAdmin admin = new RabbitAdmin(createFactory());
        return admin;
    }
    /**
     * 获取一个连接
     */
    private static Connection getConnection(){
        return connectionFactory.createConnection();
    }
    /**
     * 获取一个通道
     * 是否开启事务
     */
    public static Channel getChannel(boolean transactional){
        return defaultConnection.createChannel(transactional);
    }


}
