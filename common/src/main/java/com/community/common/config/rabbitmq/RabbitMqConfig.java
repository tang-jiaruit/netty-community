package com.community.common.config.rabbitmq;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/18 10.30
 * @Description：
 **/
public interface RabbitMqConfig {

    //连接设置
    String RABBITMQ_HOST = "39.108.110.150";
    int RABBITMQ_PORT = 5672;
    String RABBITMQ_USERNAME = "root";
    String RABBITMQ_PASSWORD = "123.abc";
    String RABBITMQ_VIRTUALHOST = "CommunityApp";
    int RABBITMQ_MAXSIZE = 1000;
    int RABBITMQ_TIMEOUT = 1000;

    //交换机信息
    String MESSAGE_DIRECT_EXCHANGE = "message_directExchange";
    //队列信息
    String Msg_QUEUE_NAME = "userMsgQueue_";

    //路由键设置  拼接用户的账号信息，保证用户有唯一的队列
    /**
     * *表示一个单词
     * #表示任意数量（零个或多个）单词
     */
    String USER_ROUTE_KEY = "user_message_";


}
