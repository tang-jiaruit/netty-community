package com.community.common.config;

/**
 * @Author： tjr
 * @CreateTime：2023-10-2023/10/31 15.41
 * @Description：定义一些静态变量
 **/
public interface ZookeeperConfig {
    String ZK_NAMESPACE = "community";
    String ZK_REGISTRY_PATH = "/registry";
    int ZK_SESSION_TIME = 30000;
    int ZK_CONNECTION_TIME = 30000;
    String  ZK_CONNECT_ADDRESS = "39.108.110.150:2181";
//String  ZK_CONNECT_ADDRESS = "127.0.0.1:2181";

}
