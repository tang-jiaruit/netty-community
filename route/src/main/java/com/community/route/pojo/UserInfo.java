package com.community.route.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/3 16.15
 * @Description：
 **/
@TableName("userinfo")
@Data
public class UserInfo {

    @TableId("id")
    private int id;
    @TableField("username")
    private String userName;
    @TableField("password")
    private String password;
    @TableField("level")
    private String level;
    @TableField("userAccount")
    private String userAccount;
    @TableField("emile")
    private String emile;
    @TableField("headPicture")
    private String headPicture;

}
