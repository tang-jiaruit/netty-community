package com.community.route.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.community.common.util.BaseResponse;
import com.community.route.dto.user.UserLoginReqVO;
import com.community.route.dto.user.UserRegistry;
import com.community.route.pojo.UserInfo;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/3 16.19
 * @Description：
 **/
public interface UserService extends IService<UserInfo> {

    //用户登录处理
    BaseResponse userLogin(UserLoginReqVO userLoginReqVO);

    BaseResponse userRegistry(UserRegistry userRegistry);

    BaseResponse sendCode(String emile);

    BaseResponse checkCode(String code, String emile);
}
