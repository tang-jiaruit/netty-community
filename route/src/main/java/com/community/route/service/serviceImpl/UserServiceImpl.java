package com.community.route.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.community.common.constant.ReqCode;
import com.community.common.pojo.ServerInfo;
import com.community.common.util.*;
import com.community.route.discovery.ServerDiscovery;
import com.community.route.discovery.UserRespVO;
import com.community.route.dto.user.UserLoginReqVO;
import com.community.route.dto.user.UserLoginResp;
import com.community.route.dto.user.UserRegistry;
import com.community.route.mapper.UserMapper;
import com.community.route.pojo.UserInfo;
import com.community.route.service.UserService;
import com.community.route.util.CodeUtil;
import com.community.route.util.EmileUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;

import java.util.concurrent.TimeUnit;

import static com.community.common.config.rabbitmq.RabbitMqConfig.*;
import static com.community.common.constant.RedisConstant.*;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/3 16.21
 * @Description：
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserInfo> implements UserService{


    @Override
    public BaseResponse userLogin(UserLoginReqVO userLoginReqVO) {

        if (userLoginReqVO==null || userLoginReqVO.getPassword() == null || userLoginReqVO.getUserAccount()==null){
            return ResultUtil.error(4001, "用户信息不正确");
        }

        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("userAccount", userLoginReqVO.getUserAccount());
        UserInfo userInfo = this.baseMapper.selectOne(wrapper);
        //如果账号不存在
        if (userInfo==null){
            return ResultUtil.error(ReqCode.NOT_FOUND_ERROR);
        }
        //如果用户存在就判断 密码是否正确 使用MD5加密
        String pas = DigestUtils.md5DigestAsHex(userLoginReqVO.getPassword().getBytes());
        if (!pas.equals(userInfo.getPassword())){
            return ResultUtil.error(4003,"密码错误");
        }

        //TODO 登录成功需要返回有用的服务地址
        ServerInfo serverInfo = ServerDiscovery.selectOneServer();

        //TODO 这里是账号信息还是id信息？
        String token = TokenUtil.getToken(userInfo.getUserAccount(), userInfo.getLevel(), String.valueOf(userInfo.getId()));

        UserLoginResp userLoginResp = new UserLoginResp();
        userLoginResp.setServerInfo(serverInfo);
        userLoginResp.setToken(token);
        UserRespVO userRespVO = new UserRespVO(userInfo.getId(),userInfo.getUserName(),userInfo.getLevel(),
                userInfo.getUserAccount(),userInfo.getEmile(),userInfo.getHeadPicture());
        userLoginResp.setUserRespVO(userRespVO);

        //将token保存到redis中 这里使用拼接的用户账号，因为账号是唯一的，所以保存的token也是唯一的，过期时间为5天
        RedisUtil.setExpire(TOKEN+userInfo.getId(),token,TOKEN_EXPIRE,TimeUnit.HOURS);
        return ResultUtil.success(userLoginResp);
    }

    @Transactional
    @Override
    public BaseResponse userRegistry(UserRegistry userRegistry) {

        //判断是否已经有账号
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("emile",userRegistry.getEmile());
        UserInfo userInfo = this.baseMapper.selectOne(wrapper);
        if (userInfo!=null){
            return ResultUtil.error(4001, "已经注册过了");
        }
        //创建用户账号信息
        Long adnIncrement = RedisUtil.getAdnIncrement(USERACCOUNTPREFIX);
        String accountEnd = CodeUtil.getAccountEnd();
        String userAccount = adnIncrement+accountEnd;
        //保存用户到数据库
        UserInfo userInfo1 = new UserInfo();
        userInfo1.setUserName(userRegistry.getUserName());
        userInfo1.setPassword(DigestUtils.md5DigestAsHex(userRegistry.getPassword().getBytes()));
        userInfo1.setEmile(userRegistry.getEmile());
        userInfo1.setUserAccount(userAccount);
        boolean save = save(userInfo1);
        if (save){
            //TODO 先为用户创建队列
            //获取用户的id信息
            UserInfo user = baseMapper.selectOne(new QueryWrapper<UserInfo>().select("id").eq("userAccount", userAccount));

            RabbitmqUtil.createQueue(Msg_QUEUE_NAME+user.getId(),true,false,false);
            RabbitmqUtil.bind(Msg_QUEUE_NAME+user.getId(),MESSAGE_DIRECT_EXCHANGE,USER_ROUTE_KEY+user.getId());
            return ResultUtil.success("注册成功");
        }
        return ResultUtil.error(4005,"注册失败");
    }

    @Override
    public BaseResponse sendCode(String emile) {
        //生成验证码 保存到redis中 有效期为5分钟
        String code = CodeUtil.getCode();
        new EmileUtil().sendEmile(emile,"验证码，5分钟内有效",code);
        RedisUtil.setExpire(USER_CODE+emile,code,CODE_EXPIRE,TimeUnit.MINUTES);
        return ResultUtil.success(code);
    }

    @Override
    public BaseResponse checkCode(String code, String emile) {

        //从redis中获取验证码
        String redisCode = RedisUtil.get(USER_CODE + emile);
        if (redisCode == null || redisCode.equals(""))
            return ResultUtil.error(4001,"没有相关信息，请重新获取");
        if (!redisCode.equals(code))
            return ResultUtil.error(4002,"验证码错误");

        return ResultUtil.success("验证码正确");
    }

}
