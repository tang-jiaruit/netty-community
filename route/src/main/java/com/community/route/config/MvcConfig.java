package com.community.route.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 15.22
 * @Description：
 **/
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    //解决跨域问题
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")  //匹配所有路径
                //设置允许认证
                .allowCredentials(true)
                //设置请求头
                .allowedHeaders("*")
                //添加运行方法
                .allowedMethods("GET","POST","POT","DELETE")
                .allowedOriginPatterns("*");
    }
}
