package com.community.route.util;

import java.util.Random;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/5 10.32
 * @Description：
 **/
public class CodeUtil {

    private static Random random = new Random();
    //获取一个6位数的验证码
    public static String getCode(){
        int i = random.nextInt(899999) + 1000000;
        return i+"";
    }

    //获取一个四位数的随机数，用于拼接用户账号
    public static String getAccountEnd(){
        return random.nextInt(9999)+"";
    }

}
