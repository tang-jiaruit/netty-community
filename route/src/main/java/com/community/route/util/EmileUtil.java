package com.community.route.util;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 20.52
 * @Description：
 **/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/**
 * 邮箱工具类
 */
@Component
public class EmileUtil {
    @Resource
    private JavaMailSender mailSender;
    public  void sendEmile(String emileAddress, String subject, String text){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("2198176247@qq.com");
        mailMessage.setTo(emileAddress);
        mailMessage.setSubject(subject);
        mailMessage.setText(text);
        mailSender.send(mailMessage);
    }
}
