package com.community.route.discovery;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/8 19.22
 * @Description：
 **/
@Data
@AllArgsConstructor
public class UserRespVO {


    private int id;
    private String userName;
    private String level;
    private String userAccount;
    private String emile;
    private String headPicture;

}
