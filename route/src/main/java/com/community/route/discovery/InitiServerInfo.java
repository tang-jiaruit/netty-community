package com.community.route.discovery;

import org.checkerframework.checker.initialization.qual.Initialized;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/5 19.31
 * @Description：
 **/
@Component
public class InitiServerInfo implements InitializingBean {

    //当spring初始化完成后执行
    @Override
    public void afterPropertiesSet() throws Exception {
        ServerDiscovery.getInstance();

    }
}
