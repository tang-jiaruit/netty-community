package com.community.route.discovery;

import com.community.common.pojo.ServerInfo;
import com.community.common.util.JsonSerialize;
import com.community.common.zookeeper.CuratorClient;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.community.common.config.ZookeeperConfig.ZK_REGISTRY_PATH;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 19.24
 * @Description：
 **/

/**
 * 这个类要使用单例模式，在项目启动的时候就需要拉去所有的节点信息，不然在用户登录的时候就太慢了
 */
public class ServerDiscovery {
    private static final Logger logger = LoggerFactory.getLogger(ServerDiscovery.class);
    private CuratorClient curatorClient;
    //使用 map 来本地保存服务信息 我们这里使用服务名来作为key，也就表明服务名不能改变，服务的host和port考研改变
    private static ConcurrentMap<String, ServerInfo> serverMap = new ConcurrentHashMap<>();
    private ServerDiscovery(){
        curatorClient = new CuratorClient();
        discoveryServer();
    }

    private static class SignDiscovery{
        private static final ServerDiscovery INSTANCE = new ServerDiscovery();
    }

    public static ServerDiscovery getInstance(){
        return SignDiscovery.INSTANCE;
    }

    /**
     * 在启动时获取所有的服务信息，本地 然后监听子节点的情况，如果发现有节点变化信息就更新本地数据
     */
    public void discoveryServer(){
        try {
            logger.info("开始获取服务信息");
            getServiceAndUpdateServer();
            //添加watch listener
            curatorClient.watchPathChildrenNode(ZK_REGISTRY_PATH, new PathChildrenCacheListener() {
                @Override
                public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
                    PathChildrenCacheEvent.Type type = pathChildrenCacheEvent.getType();
                    ChildData data = pathChildrenCacheEvent.getData();
                    switch (type){
                        case CONNECTION_RECONNECTED:
                            logger.info("重新连接zookeeper 获取最新的服务信息");
                            getServiceAndUpdateServer();
                            break;
                        case CHILD_ADDED:
                            logger.info("新增了节点，获取最新的服务信息");
                            serverAdded(data,type);
                            break;
                        case CHILD_UPDATED:
                            logger.info("更新了节点信息，获取最新的服务信息");
                            serverUpdate(data,type);
                            break;
                        case CHILD_REMOVED:
                            logger.info("有节点被删除，获取最新的服务信息");
                            serverRemove(data,type);
                            break;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ServerInfo selectOneServer(){
        if (serverMap==null && serverMap.size()==0){
            return null;
        }
        //TODO 这里要不要使用什么策略实现负载均衡等操作
        //这里先直接返回第一个服务，后面优化
        String next = serverMap.keySet().iterator().next();
        return serverMap.get(next);

    }

    //获取当前所有的服务信息,每一次调用这个都会更新 serverList 保存的信息
    private void getServiceAndUpdateServer(){
        //先清空本地数据
        serverMap.clear();
        //获取注册的服务
        try {
            List<String> children = curatorClient.getChildren(ZK_REGISTRY_PATH);
            System.out.println("=========="+children);
            for (String child : children) {
                logger.info("发现的服务：{}",child);
                //获取节点的信息
                byte[] data = curatorClient.getData(ZK_REGISTRY_PATH + "/" + child);
                ServerInfo serverInfo = JsonSerialize.deserialize(data, ServerInfo.class);
                serverMap.put(serverInfo.getServerName(),serverInfo);
            }
            logger.info("服务信息：{}",serverMap);

        } catch (Exception e) {
            logger.error("获取服务失败");
            e.printStackTrace();
        }
    }

    private void serverUpdate(ChildData data, PathChildrenCacheEvent.Type type){
        ServerInfo serverInfo = JsonSerialize.deserialize(data.getData(), ServerInfo.class);
        System.out.println(serverInfo);
        serverMap.put(serverInfo.getServerName(),serverInfo);
        System.out.println(serverMap);
    }
    //如果有节点新增就添加到map中
    private void serverAdded(ChildData data, PathChildrenCacheEvent.Type type){
        ServerInfo serverInfo = JsonSerialize.deserialize(data.getData(), ServerInfo.class);
        System.out.println(serverInfo);
        //注意这里的serverName不能重复
        serverMap.put(serverInfo.getServerName(),serverInfo);
        System.out.println(serverMap);

    }
    private void serverRemove(ChildData data, PathChildrenCacheEvent.Type type){
        ServerInfo serverInfo = JsonSerialize.deserialize(data.getData(), ServerInfo.class);
        System.out.println(serverInfo);
        serverMap.remove(serverInfo.getServerName());
        System.out.println(serverMap);
    }
}
