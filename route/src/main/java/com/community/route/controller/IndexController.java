package com.community.route.controller;
import com.community.common.util.BaseResponse;
import com.community.route.dto.user.UserLoginReqVO;
import com.community.route.dto.user.UserRegistry;
import com.community.route.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 11.29
 * @Description：用来实现用户登录，注册等功能
 **/
@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    UserService userService;
    //登录
    @RequestMapping( "/login")
    public BaseResponse login(@RequestBody UserLoginReqVO userLoginReqVO){

        return userService.userLogin(userLoginReqVO);
    }

    //注册
    @RequestMapping("register")
    public BaseResponse register(@RequestBody UserRegistry userRegistry){
        return userService.userRegistry(userRegistry);
    }

    //发送验证码
    @RequestMapping("/sendCode")
    public BaseResponse sendCode(String emile){
        return userService.sendCode(emile);

    }
    //判断验证码
    @RequestMapping("/checkCode")
    public BaseResponse checkCode(String code,String emile){
        return userService.checkCode(code, emile);
    }
    //忘记密码

    public static void main(String[] args) {
        try {
            String hostAddress = InetAddress.getLocalHost().getHostAddress();
            System.out.println(hostAddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
