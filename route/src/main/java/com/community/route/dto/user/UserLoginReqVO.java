package com.community.route.dto.user;

import lombok.Data;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 16.49
 * @Description：用户登录传入的对象信息
 **/
@Data
public class UserLoginReqVO {
    private String userAccount;
    private String password;
}
