package com.community.route.dto.user;

import com.community.common.pojo.ServerInfo;
import com.community.route.discovery.UserRespVO;
import lombok.Data;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/4 17.34
 * @Description：登录成功后返回用户 服务信息，和token
 **/
@Data
public class UserLoginResp {
    private ServerInfo serverInfo;
    private String token;
    private UserRespVO userRespVO;
}
