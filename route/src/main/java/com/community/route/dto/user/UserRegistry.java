package com.community.route.dto.user;

import lombok.Data;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/5 10.24
 * @Description：
 **/
@Data
public class UserRegistry {

    private String userName;
    private String password;
    private String emile;
}
