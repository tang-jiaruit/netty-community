package com.community.route.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.community.route.pojo.UserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author： tjr
 * @CreateTime：2023-11-2023/11/3 16.18
 * @Description：
 **/
@Mapper
public interface UserMapper extends BaseMapper<UserInfo> {

}
