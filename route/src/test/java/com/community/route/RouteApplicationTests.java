package com.community.route;

import com.community.common.pojo.ServerInfo;
import com.community.common.util.JsonSerialize;
import com.community.common.zookeeper.CuratorClient;
import com.community.route.util.EmileUtil;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import static com.community.common.config.ZookeeperConfig.ZK_REGISTRY_PATH;

@SpringBootTest(classes = RouteApplication.class)
@RunWith(SpringRunner.class)
class RouteApplicationTests {


    /**
     * 这里注意使用 @Resource 会根据name来寻找bean 这里要使用 StringRedisTemplate 那么变量名就必须是 stringRedisTemplate
     * 不能是redisTemplate 不然就会报错
     */
    @Resource
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    EmileUtil emileUtil;

    @Test
    void contextLoads() {
        System.setProperty("zookeeper.sasl.client", "false");
        CuratorClient curatorClient = new CuratorClient();
        try {
            List<String> children = curatorClient.getChildren("/registry");
            System.out.println(children+"===========");
            for (String child : children) {
                byte[] data = curatorClient.getData(ZK_REGISTRY_PATH + "/" + child);
                ServerInfo serverInfo = JsonSerialize.deserialize(data, ServerInfo.class);
                System.out.println(serverInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void test(){
        CuratorClient curatorClient = new CuratorClient();
        byte[] serialize = JsonSerialize.serialize(new ServerInfo("5", 2, "9999"));
        try {
//            curatorClient.updatePathData("/registry/communityServer01 0000000007",serialize);
//            curatorClient.createPathData("/registry/test",serialize);
            curatorClient.deletePath("/registry/test");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
